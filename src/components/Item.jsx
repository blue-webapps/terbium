import React, { useState, useEffect } from 'react';
import { Button, Form, Segment, TextArea } from 'semantic-ui-react';
import { useDebouncedCallback } from 'use-debounce';

import todos from '../lib/todos';

const Item = props => {
  const { id, notes: _notes, source } = props;

  const [notes, notesSet] = useState(_notes);

  const onRemove = () => todos.remove(id);
  const onTop = () => todos.top(id);
  const onBottom = () => todos.bottom(id);
  const [onUpdate] = useDebouncedCallback(e => todos.update({ id, notes, source, ...e }), 500);

  const onChange = (e, d) => notesSet(d.value);

  useEffect(() => {
    onUpdate({ notes });
  }, [notes]);

  const links = [...new Set(notes.match(/(https?:\/\/[^\s]+)/g))].map(a => (
    <Button color="blue" key={a} target="_blank" href={a} style={{ padding: '0.5em', marginTop: '0.5em' }}>
      {a}
    </Button>
  ));

  return (
    <Segment>
      <Form>
        <Form.Group style={{ display: 'flex', marginBottom: '0.5em', marginRight: '0em' }}>
          <Form.Field style={{ flex: 2, marginBottom: 0 }}>
            <TextArea value={notes} onChange={onChange} />
          </Form.Field>
          <div>
            <Button color="green" icon="angle double up" onClick={onTop} />
            <Button color="orange" icon="angle double down" onClick={onBottom} />
            <Button color="red" icon="x" onClick={onRemove} />
          </div>
        </Form.Group>
        {!!links.length && links}
      </Form>
    </Segment>
  );
};

export default Item;
