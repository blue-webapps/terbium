import React, { useState, useEffect } from 'react';
import { Button, Container, Header, Segment } from 'semantic-ui-react';

import Item from './Item';
import todos from '../lib/todos';

const ToDoList = () => {
  const [list, listSet] = useState([]);

  useEffect(() => {
    listSet(todos.get());
    todos.on('update', listSet);

    return () => {
      todos.removeListener('update', listSet);
    };
  }, []);

  const items = list.map(({ id, notes, source }) => <Item key={id} id={id} notes={notes} source={source} />);

  const onAdd = () => {
    todos.add({ notes: '', source: '' });
  };

  return (
    <Container>
      <Segment basic>
        <Header> terbium is a simple, private todo list </Header>
        {items}
        <div style={{ position: 'absolute', top: 4, right: 4 }}>
          <Button primary icon="plus" onClick={onAdd} />
        </div>
      </Segment>
    </Container>
  );
};

export default ToDoList;
