const path = require('path');
const fs = require('fs');
const assert = require('assert');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const BuildIconsFromSVGPlugin = require('./webpack-plugins/build-icons-from-svg-plugin');

const { title } = JSON.parse(fs.readFileSync('./package.json'));
assert(title, 'No title specified in package.json');

const config = {
  entry: {
    index: './src/index.jsx'
  },
  output: {
    path: path.join(__dirname, 'public'),
    filename: '[name].[hash].js',
    sourceMapFilename: '[name].[hash].map'
  },
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },
  plugins: [
    new CleanWebpackPlugin(),

    new BuildIconsFromSVGPlugin({ svgPath: 'src/icon.svg', outputDir: 'public/icons' }),

    new CopyWebpackPlugin([
      { from: 'semantic/dist/semantic.min.css', to: 'style/' },
      { from: 'semantic/dist/themes', to: 'style/themes' }
    ]),

    new HtmlWebpackPlugin({
      title,
      template: path.join(__dirname, 'src', 'index.ejs')
    })
  ],
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['@babel/preset-react', '@babel/preset-env']
        }
      }
    ]
  },

  resolve: {
    extensions: ['.js', '.jsx']
  }
};

module.exports = config;
