const sharp = require('sharp');
const fs = require('fs-extra');
const toIco = require('to-ico');

const log = (...args) => console.log('build-icons-plugin', ...args);

const main = async (svgPath, outputDir) => {
  try {
    log(`Create output dir for icons '${outputDir}...'`);
    fs.emptyDirSync(outputDir);

    log(`Building png's from ${svgPath}...`);
    await Promise.all(
      [16, 32, 48, 64, 128].map(s =>
        sharp(svgPath)
          .resize(s, s)
          .toFile(`${outputDir}/icon-${s}.png`)
      )
    );

    log(`Loading png's...`);
    const files = [16, 32, 48, 64].map(s => fs.readFileSync(`${outputDir}/icon-${s}.png`));

    log(`Building ico...`);
    await toIco(files).then(buf => {
      fs.writeFileSync(`${outputDir}/favicon.ico`, buf);
    });
    log(`Finished building icons`);
  } catch (e) {
    console.error(e);
  }
};

class BuildIconsFromSVGPlugin {
  constructor(options) {
    this.options = options;
  }

  apply(compiler) {
    compiler.hooks.emit.tapAsync('BuildIconsFromSVGPlugin', (compilation, callback) => {
      const { options } = this;
      main(options.svgPath, options.outputDir);
      callback();
    });
  }
}

module.exports = BuildIconsFromSVGPlugin;
